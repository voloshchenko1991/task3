package com.dvo;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Random;
import java.util.Scanner;

public class GameOfLife {
    public static final int DIMENSION = 52;
    public static final int ITERATIONS = 1000;
    public static final int DELAY_MS = 100;
    public static int[][] field = new int[DIMENSION][DIMENSION];
    public static int[][] neighboursField = new int[DIMENSION][DIMENSION];
    public static int[][] stepField = new int[DIMENSION][DIMENSION];
    public static int[][] testField = new int[DIMENSION][DIMENSION];
    public static int[][] testField2 = new int[DIMENSION][DIMENSION];
    public static int iteration = 0;
    public static int[][] flush = new int[DIMENSION][DIMENSION];

    public static void main(String[] args) throws IOException, InterruptedException {
        // write your code here
        initFill(field);
        putNeighboursCountToField(field, neighboursField);
        copyAuxLines(neighboursField);
        drawField(field, neighboursField, true);

        for (int i = 0; i < ITERATIONS; i++) {
            Thread.sleep(DELAY_MS);
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            step(field, stepField, neighboursField);
            field = stepField;
            stepField = new int[DIMENSION][DIMENSION];
        }
    }

    private static void drawField(int[][] inputField, int[][] neighboursField, boolean drawWithLetters) {
        System.out.println();
        int checkInput;
        int checkNeighbour;
        int debug = 1;
        for (int i = debug; i < DIMENSION - debug; i++) {
            for (int j = debug; j < DIMENSION - debug; j++) {
                checkNeighbour = neighboursField[i][j];
                checkInput = inputField[i][j];
                if (drawWithLetters) {
                    if (checkInput != 0) {
                        switch (checkNeighbour) {
                            case 0:
                                System.out.print('A');
                                break;
                            case 1:
                                System.out.print('B');
                                break;
                            case 2:
                                System.out.print('C');
                                break;
                            case 3:
                                System.out.print('D');
                                break;
                            case 4:
                                System.out.print('E');
                                break;
                            case 5:
                                System.out.print('F');
                                break;
                            case 6:
                                System.out.print('G');
                                break;
                            case 7:
                                System.out.print('H');
                                break;
                            case 8:
                                System.out.print('I');
                                break;
                        }
                    } else {
                        System.out.print(' ');
                    }
                } else {
                    if (checkNeighbour != 0) {
                        System.out.print('X');
                    } else {
                        System.out.print(' ');
                    }
                }
                if (j == (DIMENSION - 2)) {
                    System.out.println();
                }
            }
        }
    }


    private static void initFill(int[][] inputField) {
        Random random = new Random();
        for (int i = 1; i < DIMENSION - 1; i++) {
            for (int j = 1; j < DIMENSION - 1; j++) {
                if (random.nextInt(2) == 0) {
                    inputField[i][j] = 0;
                } else {
                    inputField[i][j] = 1;
                }
            }
        }
        copyAuxLines(inputField);

    }

    private static void copyAuxLines(int[][] inputField) {
        for (int i = 1; i < DIMENSION - 1; i++) {
            inputField[0][i] = inputField[DIMENSION - 2][i];
            inputField[DIMENSION - 1][i] = inputField[1][i];
            inputField[i][0] = inputField[i][DIMENSION - 2];
            inputField[i][DIMENSION - 1] = inputField[i][1];
        }
    }

    private static void step(int[][] inputField, int[][] outputField, int[][] neighboursField) {

        for (int i = 1; i < DIMENSION - 1; i++) {
            for (int j = 1; j < DIMENSION - 1; j++) {
                if (neighboursField[i][j] == 3) {
                        outputField[i][j] = 1;
                }
                if ((neighboursField[i][j] < 2) && (neighboursField[i][j] > 3)) {
                    outputField[i][j] = 0;
                }
                if (neighboursField[i][j] == 2) {
                    if (inputField[i][j] == 1) {
                        outputField[i][j] = 1;
                    }
                }
            }
        }
        copyAuxLines(outputField);
        putNeighboursCountToField(outputField, neighboursField);
        copyAuxLines(neighboursField);
        drawField(outputField, neighboursField, true);
        System.out.print("Iteration: ");
        System.out.println(++iteration);
    }


    private static void putNeighboursCountToField(int[][] inputField, int[][] outputField) {
        for (int i = 1; i < (DIMENSION - 1); i++) {
            for (int j = 1; j < (DIMENSION - 1); j++) {
                outputField[i][j] = getLiveNeighbours(i, j, inputField, true);
            }
        }

    }

    private static int getLiveNeighbours(int testedRaw, int testedColumn, int[][] inputField, boolean getForZeroCells) {
        int x = testedRaw;
        int y = testedColumn;
        int result = 0;
        if ((inputField[x][y] != 0) || (getForZeroCells)) {
            for (int i = x - 1; i < x + 2; i++) {
                for (int j = y - 1; j < y + 2; j++) {
                    if ((i == x) && (j == y)) {
                        continue;
                    }
                    if (inputField[i][j] > 0) {
                        result++;
                    }
                }
            }

        }
        return (result);
    }
}
