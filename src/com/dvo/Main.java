package com.dvo;

import java.io.IOException;

/**
 * Created by Paradox on 02.07.2017.
 */
public class Main {
    public static final int ITERATIONS = 1000;
    public static final int DELAY_MS = 100;

    public static void main (String ... args )throws IOException, InterruptedException{
        Universe universe = new Universe(50);
        for (int i = 0; i < ITERATIONS; i++) {
            Thread.sleep(DELAY_MS);
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            universe.step();
        }
    }
}
