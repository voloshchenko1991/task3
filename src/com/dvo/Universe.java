package com.dvo;


import com.sun.corba.se.impl.orbutil.DenseIntMapImpl;

import java.util.Random;

public class Universe {
    private int field[][];
    private int nextField[][];
    private int neighbourField[][];

    private int DIMENSION;
    private final char TEXTURE[] = {'A','B','C','D','E','F','G','H','I'};

    Universe() {
        DIMENSION = 50;
        init();
    }

    Universe(int dimension) {
        DIMENSION = dimension;
        init();
    }

    private void init() {
        field = new int[DIMENSION][DIMENSION];
        nextField = new int[DIMENSION][DIMENSION];
        neighbourField = new int[DIMENSION][DIMENSION];
        randomize();
        fillFieldOfNeighbours();
    }

    public void step(){
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                if((field[i][j] == 0)&&(neighbourField[i][j] == 3)){
                    nextField[i][j] = 1;
                }
                if((field[i][j] == 1)&&((neighbourField[i][j] < 2)||(neighbourField[i][j] > 3))){
                    nextField[i][j] = 0;
                }
            }
        }
        System.arraycopy(field,0,nextField,0,DIMENSION);
        fillFieldOfNeighbours();
        printArray(neighbourField);
    }

    private void fillFieldOfNeighbours() {
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                neighbourField[i][j] = checkCountOfNeighbours(i,j);
            }
        }
    }


    private int checkCountOfNeighbours(int x, int y) {
        int result = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if ((i == x) && (j == y)) {
                    continue;
                } else {
                    if (field[convertCoordinates(i)][convertCoordinates(j)] == 1) {
                        result++;
                    }
                }
            }
        }
        return (result);
    }

    private int convertCoordinates(int coordinate) {
        int result;
        if (coordinate < 0) {
            result = DIMENSION - 1;
        } else if (coordinate == DIMENSION) {
            result = 0;
        } else {
            result = coordinate;
        }
        return (result);
    }

    private void randomize() {
        Random random = new Random();
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                field[i][j] = random.nextInt(2);
            }
        }
    }

    private void printArray(int[][] array) {
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                if(field[i][j] == 0){
                    System.out.print(' ');
                }else {
                    System.out.print(TEXTURE[array[i][j]]);
                }
            }
            System.out.println();
        }
        System.out.println();
    }

}
